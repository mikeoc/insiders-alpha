# Insiders Alpha

What what exactly *is* the &ldquo;insider's alpha?&rdquo; And why do only insiders get to use it? It is a measure of portfolio performance that extends the famous alpha of Michael Cole Jensen in such a way as to be applicable to portfolios of stocks and other securities whose managers from time to time resort to going into cash in a substantial way&nbsp;[1].

## The Need

Jensen basically introduced an intercept, his alpha, into the Capital Asset Pricing Model (CAPM) regression model for the excess return of a security or portfolio of securities, the &ldquo;excess return&rdquo; being in this context the return in excess of that of cash. But for every period during which the portfolio is entirely in cash&mdash; this is an extreme case that is offered as an example&mdash; the excess return would be zero, meaning that the excess returns of such periods wouldn't even be distributed but would be bunched up on top of each other. That would bring about a &ldquo;mixed&rdquo; distribution of portfolio excess returns overall. But the regression model's independent variable would remain the excess return on the marketplace which does *not* have a mixed distribution. Therefore, were Jensen's CAPM-inspired regression model to be applied in such circumstances the residuals would be huge and the t-statistic associated with his alpha would not be distributed as Student's t distribution&mdash; meaning that significance would not be determinable.

In the face of those particulars we cannot simply fall back on the traditional measures such as the cumulative return or the Sharpe ratio. If for example we employ a momentum or trend-following strategy, that might cause us to hold securities that move up and down in synchrony with the market but in a magnified and volatile way&mdash; &ldquo;high-beta.&rdquo; Since the market generally goes up over any lengthy period of study then such securities would automatically be expected to have gone up even more and to have been favored accordingly by our momentum or trend-following strategy. But they would also go down even more were the market to decline, and so we can't rely on the bare portfolio return as a figure of merit because it would reward the indiscriminate resort to high-beta securities notwithstanding the attendant volatility.

Now the Sharpe ratio does have the meritorious characteristic of amounting to a risk-adjusted return. Furthermore, when security excess returns are well represented by the aforementioned CAPM regression model then the Sharpe ratio is independent of the betas, so it doesn't then reward the risky use of high betas. But&hellip; as we have seen, the regression residuals are very large if cash is ever a very substantial part of the portfolio. So in that circumstance the Sharpe ratio loses its lack of dependence on the betas.

## Concepts and Use

The conceptual development of the insider's alpha and the basic description of the algorithm for it are  described in just about six pages of a working paper that is now available online&nbsp;[2]. Insider's alpha does entirely avoid the problems cited above regarding the mismatch between the mixed distribution of portfolio excess returns and the distribution of the market's returns.

For the most part insiders are the only people who can wield the insider's alpha because its computation requires detailed knowledge of all of the positions held, hence the nomenclature (which is hopefully also a bit &ldquo;catchy&rdquo;). Or equivalently, if the management of the portfolio is entirely rule-based then it would be sufficient to know the rule. Yes, the insider's alpha is indeed intended to help quantitative analysts decide if a trading or asset allocation scheme that they have been working on is suitable for use. To that end, bias-corrected and accelerated confidence interval lower (and upper) bounds for the insider's alpha are also computed&nbsp;[3].

## Technical Details

The coding for this project is presently in Python with Numpy, Matplotlib and Joblib. The algorithm is a bit CPU-intensive and so it incorporates parallel processing. The code runs with Python2.7 and Python3.6 with compatible dependencies. The included file `insidersalpha.py` is a little library of functions, all of which are called by `ia.py`, invoked by `python ia.py`. Charts are printed in windows on the screen; numerical results go to the terminal. X11 forwarding can be used to see the charts if the code is run on a server (with server and client both properly configured for X11 forwarding enablement).

The code as it now stands is not suitable for dealing with short sales, but only small modifications would be necessary to improve on that. One point of contention is the matter of interest on the proceeds of short sales. Generally retail investors are not paid any such interest but the circumstances of institutional investors wielding a great deal of capital may be more complicated in that regard. Currently the code pays interest on all cash. The cited working paper explains the long-only and long-cash context in which the insider's alpha was developed&nbsp;[2].

Presently the test datasets are `Audit_mom_french_indu10.csv` and `Audit_mom_shiller_sp_composite.csv` which are audit files taken from www.thetradedportfolio.com/momentum/. Other audit files may become available there. It is understood that users will have input formats of their own and will otherwise substantially modify and customize the code.

## Status and Request for Review

There has not been extensive testing of the code on various platforms because the insider's alpha is newly proposed. We could call it an alpha version (pun). It is disclosed here in order to facilitate review and improvement by interested quantitative financial analysts and programmers. Critical comments are most welcome, about the details and the generalities. Would anyone like to contribute a version in R? A pull request? Write to the author at the email address in the comment at the top of `ia.py`. Anyone with something to contribute may be made a member&mdash; able to raise issues, push requests, etc.

## References

[1] Michael Cole Jensen. The performance of mutual funds in the period 1945–64. *The Journal of Finance*, 23(2):389–416, May 1968 https://onlinelibrary.wiley.com/doi/epdf/10.1111/j.1540-6261.1968.tb00815.x

[2] Michael O'Connor. Fund and subportfolio momentum. *SSRN*, March 2019 http://ssrn.com/abstract=3364124

[3] Bradley Efron. Better bootstrap confidence intervals. *Journal of the American Statistical Association*, 82(397):171–200, 1987 https://apps.dtic.mil/dtic/tr/fulltext/u2/a150798.pdf

