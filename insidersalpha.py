'''
These are functions supporting the insider's alpha computations.
'''

import numpy as np
np.seterr(all='raise')
import os, datetime
from scipy.stats import norm
import statsmodels.api as sm


#########################################################################################################################

def shuffle(indexlist, shuffles, betas, positions, securityreturns, cashreturns, benchmarkreturns, justabnormal = False):
    
    '''
    This function does the main work, of computing actual and expected returns from given 
    Monte Carlo simulations. 

        indexlist           The result of random sampling with replacement, one time, of
                            the date axis index.
                            --- type: ndarray; shape: #dates
        shuffles            The result of random sampling without replacement, shuffling, many
                            times, of the date axis index.
                            --- type: Python list of ndarrays; "shape": #shuffles x #dates
        betas               From OLS, the betas of buy-and-hold positions in the securities 
                            with respect to the benchmark.
                            --- type: Python list; length: #securities 
        positions           The positions held.
                            --- type: ndarray; shape: #dates x #securities
        securityreturns     The returns on each security if bought and held, expressed as ratios
                            per period.
                            --- type: ndarray; shape: #dates x #securities
        cashreturns         The returns on cash, expressed as ratios per period.
                            --- type: ndarray; shape: #dates
        benchmarkreturns    The returns on cash, expressed as ratios per period.
                            --- type: ndarray; shape: #dates
        justabnormal        Controls what is output.
                            --- type: Python boolean
    '''

    ##
    # Create sets of positions shuffled with respect to date axis.
    ##

    # --- shape: #shuffles x #dates
    sampleshuffles = np.array( [ indexlist[ ind ] for ind in shuffles ] )

    # --- shape: #dates x #securities x #shuffles
    randpositions = np.zeros( ( indexlist.shape[0], positions.shape[1], sampleshuffles.shape[0] ), dtype=float )
    i = 0
    while i < sampleshuffles.shape[0]:
        randpositions[ : , : , i ] = positions[ sampleshuffles[i] , : ]
        i += 1
    assert randpositions.shape == ( indexlist.shape[0], positions.shape[1], sampleshuffles.shape[0] )


    ##
    # Use randpositions and allbetas to get f * beta product (fbeta) where f is the
    # total fraction of equity held as long positions and beta is the position-weighted
    # mean of the subportfolio betas.
    ##

    # Every row of allbetas has the same list of betas.
    # --- shape: #dates x #securities
    allbetas = np.array(indexlist.shape[0] * [betas] )
    assert allbetas.shape == ( indexlist.shape[0], positions.shape[1] )

    # --- shape: #dates x #shuffles
    fbeta = np.sum( allbetas[:,:,None] * randpositions, axis=1 )
    assert fbeta.shape == ( indexlist.shape[0], sampleshuffles.shape[0] )


    ##
    # Compute the returns on the actually-held positions as ratios per period.
    ##

    # --- shape: #dates
    actualreturns = cashreturns[indexlist] * ( 1.0 - np.sum( positions[indexlist], axis=1 ) ) +\
        np.sum( positions[indexlist] * securityreturns[indexlist], axis=1 )


    ##
    # Compute the expected returns using the held positions but with the regression returns,
    # as ratios per period.
    ##

    # --- shape: #dates x #shuffles
    expectedreturns = cashreturns[indexlist].reshape(-1,1) +\
        fbeta * ( benchmarkreturns - cashreturns )[indexlist].reshape(-1,1)

    ##
    # Combine terms for later use.
    ##

    # --- shape: #dates
    actuallogtotreturns = np.log( actualreturns )

    # --- shape: 1 (scalar).
    meanactuallogtotreturns = np.mean( np.log( actualreturns ), axis=0)

    # --- shape: 1 (scalar).
    meanmeanlogexpectedreturns = np.mean( np.mean( np.log( expectedreturns ) , axis=0), axis=0 )

    # --- shape: #shuffles
    meanlogexpectedreturns = np.mean( np.log( expectedreturns ) , axis=0)


    if justabnormal:
        return meanactuallogtotreturns - meanmeanlogexpectedreturns
    else:
        return meanactuallogtotreturns, meanmeanlogexpectedreturns, meanlogexpectedreturns, actuallogtotreturns

#########################################################################################################################

def acquire_ttp_audit_data( csvfilename ):

    '''
    On the website theTradedPortfolio.com "audit" files are available that are outputs from
    runs that apply a certain portfolio management scheme to lists of securities. This
    function derives, from any given audit file, dates and returns on cash and on buy-and-hold
    positions in each of the securities.

        csvfilename     The name of the audit file... assumes that it's in the same directory as
                        this file.
                        --- type: str
    '''

    ##
    # Acquire the data.
    ##

    PTHNM = os.path.dirname(os.path.abspath(__file__))
    DATA = np.loadtxt(
        os.path.join( PTHNM, csvfilename ),
        converters={-1: lambda s: 0.0 },
        delimiter=",", comments="#"
    )

    # Delete the last two columns which are a portfolio returns column
    # and a spreadsheet formula column (which has been turned into zeros).
    DATA = DATA[ : , 0 : -2 ]

    # Get the number of securities. There will be a position and a price
    # column for each security, plus a date column and a cash column.
    numsec = int( ( DATA.shape[1] - 2 ) / 2 )

    # Get the names of the securities.
    # --- length: #securities
    securitynames = None
    with open(os.path.join( PTHNM, csvfilename ), 'r') as infile:
        for line in infile:
            if line.startswith('#'):
                securitynames = line.split(',')[ 2 : 2 + numsec ]
            else:
                break

    # Get returns as ratios, from cumulative returns.
    # --- shape: #dates
    dates = DATA[ : , 0 ]

    # --- shape: #dates x (#securities + 1)
    cumreturns = DATA[ : , 1 : numsec + 2 ]
    cumreturnsprior = np.insert(
        np.delete( cumreturns, -1, axis=0 ), 0, cumreturns[0], axis=0
    )

    # The first row of returns would be just 1's so we delete it. 
    # --- shape: (#dates - 1) x (#securities + 1)
    returns = ( cumreturns / cumreturnsprior )[ 1 : ]

    # --- shape: #dates - 1
    cashreturns = returns[ : , 0 ]

    # --- shape: (#dates - 1) x #securities
    securityreturns = returns[ : , 1 : ]

    # --- shape: #dates - 1
    benchmarkreturns = np.mean( securityreturns, axis = 1 )

    # Positions in a given row are by convention, in a theTradedPortfolio.com
    # website audit file, those that are to be held over the NEXT
    # period so that when computing the cumulative return we need to
    # use the positions of the previous day with the security returns
    # of the current day. 
    # --- shape: (#dates - 1) x #securities
    positions = DATA[ : , numsec + 2 : ][ 0 : -1 ]

    # --- shape: #dates - 1
    dates = dates[ 1 : ]

    datetimedates = [ datetime.date( int( str( int( d ) )[ 0 : 4 ] ), int( str( int( d ) )[ 4: 6 ] ), int( str( int( d ) )[ 6: 8] ) ) for d in dates ]
    daysperperiod = float( datetimedates[-1].toordinal()-datetimedates[0].toordinal() ) / float( len(datetimedates) )

    if daysperperiod < 34.0 and daysperperiod > 27:
        frequency = 'monthly'
        factor = 12.0
        plural = 'months'
    elif daysperperiod < 8.0 and daysperperiod > 6.0:
        frequency = 'weekly'
        factor = 52.0
        plural = 'weeks'
    elif daysperperiod < 1.5 and daysperperiod > 0.99:
        frequency = 'daily'
        factor = 250.0
        plural = 'days'
    else:
        print('Could not determine the frequency of the data. Terminating...')
        sys.exit()

    return datetimedates, cashreturns, securitynames, securityreturns, positions, benchmarkreturns, frequency, factor, plural

#########################################################################################################################

def plot_cdf( values, factor, title, xlabel, statistic, note):

    '''
    This simply plots cdf's.

        values      The cdf of these values is to be plotted.
                    --- type: ndarray; shape: 1D
        factor      If None then the values are to be plotted on the x axis;
                    if not None then factor is to be used to annualize returns.
                    --- type: float
        title       Goes on top of the plot.
                    --- type: str
        xlabel      The x-axis label.
                    --- type: str
        statistic   Sets the location of a vertical line.
                    -- type: float
        note        Is printed on the plot.
                    --- type: str

    '''

    import matplotlib.pyplot as plt

    values = np.sort(values)
    mu = np.mean(values)
    std = np.std(values)
    if factor == None:
        x = values
    else:
       x = 100.0 * ( np.exp( factor * values ) - 1.0 ) 
    pts = np.arange(values.shape[0]).astype(float) / ( float( values.shape[0] ) - 1.0 )
    vals = [ norm.cdf( xx, mu, std) for xx in values ]
    plt.axvline(x = statistic, color='tab:brown' )
    plt.axhline(y = 0.5, color='y')
    plt.figtext( 0.15, 0.63, note, color='tab:brown' )
    plt.plot( x, pts , '.', label='data cdf', markersize=8 )
    plt.plot( x, vals , '.', label='parametric cdf', markersize=1 )
    plt.legend()
    plt.ylabel( 'cdf', fontsize=14 )
    plt.title( title )
    plt.xlabel( xlabel, fontsize=14 )
    plt.show()
    plt.clf()
    plt.cla()
    plt.close()

#########################################################################################################################

def chart(chartdates, series1, seriestype1, series2, seriestype2, series3, seriestype3, title=None, note=None):

    '''
    This plots cumulative return versus time.

        chartdates      Period ending dates.
                        --- type: list of datetime dates
        seriesn         Data series to be plotted (first value must be 1.0 in order for the ylabel that is fixed in code here to be
                        correct).
                        --- type: list of floats
        seriestypen     Labels for the plots.
                        --- type: str
        title           Chart title.
                        --- type: str
        note            A note that appears on the chart.
                        --- type: str
        
    '''

    import matplotlib.pyplot as plt

    assert len( set( [len(chartdates), len(series1), len(series2)] ) ) == 1
    plt.plot( chartdates, series1, label=seriestype1 )
    plt.plot( chartdates, series2, label=seriestype2 )
    plt.plot( chartdates, series3, label=seriestype3 )
    plt.yscale( 'log' )
    plt.ylabel( 'Cumulative Return on $1' )
    plt.legend(loc='upper left')
    plt.title(title)
    if note is not None: plt.figtext( 0.15, 0.60, note, color='tab:brown' )
    plt.show()
    plt.clf()
    plt.cla()
    plt.close()

#########################################################################################################################

def betas(cashreturns, securitynames, securityreturns, benchmarkreturns):

    '''
    Get OLS betas for each security.

        cashreturns         Expressed as cumulative return ratios per period.
                            --- type: ndarray; shape: #dates
        securitynames       No explanation needed.
                            --- type: Python list of strings; length: #securities
        securityreturns     Expressed as cumulative return ratios per period, one
                            for each security, bought and held.
                            --- type: ndarray; shape: #dates x #securities
        benchmarkreturns    Expressed as cumulative return ratios per period.
                            --- type: ndarray; shape: #dates
    '''

    betas = []
    print('')
    print( 'Here are the betas of the securities:' )
    print('')
    print( '  SECURITY{}{}'.format( ( 18 - len( 'SECURITY' ) ) * ' ', 'BETA' ) )
    for i in range(securityreturns.shape[1]):
        fit = sm.OLS( securityreturns[ : , i ] - cashreturns, benchmarkreturns - cashreturns ).fit()
        beta = fit.params[0]
        betas += [beta]
        print( '  {}{}{}'.format( securitynames[i], ( 18 - len( securitynames[i] ) ) * ' ', beta ) )
    print('')

    return betas

#########################################################################################################################

def acceleration(jdist, anndist, lessppf, numsamples, alpha=0.05):

    '''
    This implements the accelertion part of Bradley Efron's bias-corrected
    and accelerated (BCa) bootstrap method--- using jackknives.

        jdist       The distribution of none-annualized abnormal returns, one
                    value per jackknife sample.
                    --- shape: #dates  
        anndist     The distribution of annualized abnormal returns, one
                    value per bootstrap sample.
                    --- shape: #samples
        lessppf     The result of appling the inverse cdf to the fraction of
                    bootstrap samples for which the computed insider's alpha
                    was less than that of the uncorrected value. This is the
                    bias correction.
                    --- type: float
        numsamples  The number of bootstrap samples taken.
                    --- type: int
        alpha       The statistical level of significance.
                    --- type: float
    '''

    jmean = np.mean(jdist)
    anumerator = np.sum( [ ( jmean - jd )**3 for jd in jdist ] )
    adenominator = 6.0 * np.sum( [ ( jmean - jd )**2 for jd in jdist ] ) ** (3/2)
    a = anumerator / adenominator
    adjalpha = lambda z, unadjalpha: norm.cdf( z + ( z + norm.ppf(unadjalpha) ) / ( 1.0 - a * ( z + norm.ppf(unadjalpha) ) ) )
    lowerbnd95pct = anndist[ int(  adjalpha(lessppf, alpha/2) * numsamples ) ]
    upperbnd95pct = anndist[ int(  adjalpha(lessppf, 1-alpha/2) * numsamples ) ]

    return lowerbnd95pct, upperbnd95pct

#########################################################################################################################

