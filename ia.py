'''
This code, in Python with Numpy and Matplotlib, computes Mike O'Connor's "insider's alpha" portfolio performance
measure. It is an extension of Michael Cole Jensen's famous alpha [1]. The need for something like the insider's alpha
arises particularly with portfolio management schemes that involve substantial moves into cash during
parlous times, which bring about a mixed distributions of portfolio returns that deviate very substantially
from the lognormal form.

In those circumstances Jensen's alpha can still be computed but the residuals are huge and the t-statistic associated
with it is not distributed as Student's t distribution and so significance is not determinable. With the code presented
below confidence intervals are computed using Bradley Efron's bias-corrected and accelerated (BCa) bootstrap
procedure [2].

The conceptual development of the insider's alpha and the basics of the algorithm for it are described in
just about six pages of a working paper that is now available online [3].

Correspondance (Mike O'Connor): mike@mocpa.com.
Web pages: www.mocpa.com & www.thetradedportfolio.com

------------------------

[1] Michael Cole Jensen. The performance of mutual funds in the period
1945-64. The Journal of Finance, 23(2):389-416, May 1968
https://onlinelibrary.wiley.com/doi/epdf/10.1111/j.1540-6261.1968.tb00815.x

[2] Bradley Efron. Better bootstrap confidence intervals. Journal of the
American Statistical Association, 82(397):171-200, 1987
http://hal.case.edu/~robrien/Efron87Better%20Bootstrap%20Confidence%20Intervals.pdf

[3] Michael C. O'Connor. Fund and subportfolio momentum. SSRN, March 2019
http://ssrn.com/abstract=3364124 Write to the author mike@mocpa.com for a
copy if you are unable to download from SSRN.

'''

#########################################################################################################################

def compute(filename, alpha=0.05, numshuffles=1000, numsamples=1000 ):

    '''
    This computes the insider's alpha and its confidence interval bounds.

        filename        The local file that is the source of the data.
                        --- format: csv; type: string
        alpha           The level of significance (not the insider's alpha).
                        --- type: float
        numshuffles     The number of Monte Carlo permutations used to compute the expected return.
                        --- type: int
        numsamples      The number of random samples with replacement used in the BCa bootstrap
                        computation of the upper and lower bounds of the insider's alpha.
                        --- type: int
    '''

    from tqdm import tqdm
    import numpy as np
    np.seterr(all='raise')
    from scipy.stats import norm
    from joblib import Parallel, delayed, cpu_count
    import insidersalpha as ia
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()

    ##
    # Acquire the returns and positions data and determine the data frequency. It's anticipated that users will rewrite this
    # section to accomodate their own inputs, providing values for the following variable names: dates, cashreturns,
    # securitynames, securityreturns, positions, benchmarkreturns, frequency, factor and plural.
    ##

    dates, cashreturns, securitynames, securityreturns, positions, benchmarkreturns, frequency, factor, plural =\
         ia.acquire_ttp_audit_data( filename )

    #-------------------------------------------------------------------------------------------------------------------------

    ##
    # Get OLS betas for each security.
    ##

    betas = ia.betas(cashreturns, securitynames, securityreturns, benchmarkreturns)

    #-------------------------------------------------------------------------------------------------------------------------

    ##
    # Get the Monte Carlo permutation shuffles created first so that they will
    # all be the same every time shuffle() is called.
    ##

    # --- shape: #dates
    indexrange = np.arange( cashreturns.shape[0] )

    #--- "shape": #shuffles x #dates
    shuffles = [  np.random.choice( indexrange, indexrange.shape[0], replace=False ) for i in range(numshuffles) ]

    #-------------------------------------------------------------------------------------------------------------------------

    ##
    # Get the insider's alpha terms calculated. Later below we do the
    # BCa bootstrap estimate of confidence interval bounds.
    ##

    meanactuallogtotreturns, meanmeanlogexpectedreturns, meanlogexpectedreturns, actuallogtotreturns = \
        ia.shuffle(indexrange, shuffles, betas, positions, securityreturns, cashreturns, benchmarkreturns)

    #-------------------------------------------------------------------------------------------------------------------------

    ##                
    # Do a bias calculation with bootstrapping, using all available logical cpu's.
    ##

    # --- "shape": #samples x #dates
    samples = [ np.random.choice( np.arange(cashreturns.shape[0]), cashreturns.shape[0], replace=True ) for i in range(numsamples) ]

    print( "Finding the bootstrap sample insider's alphas..." )

    # --- shape: #samples
    dist = np.sort(
        np.array(
            Parallel(n_jobs=cpu_count())(
                delayed(ia.shuffle)(s, shuffles, betas, positions, securityreturns, cashreturns, benchmarkreturns, justabnormal=True) 
                for s in tqdm(samples)
            )
        )
    )

    print('')

    anndist = 100.0 * ( np.exp( factor * dist ) - 1.0 )

    # --- type: float 
    lessppf = norm.ppf(
        (
            float(dist[ dist < meanactuallogtotreturns - meanmeanlogexpectedreturns ].shape[0]) +
            float(dist[ dist == meanactuallogtotreturns - meanmeanlogexpectedreturns ].shape[0]) / 2.0 
        ) / float(numsamples)
    )

    #-------------------------------------------------------------------------------------------------------------------------

    ##
    # Now do some jackknives, in preparation for computing the acceleration.
    # There will be one calculation for every date--- for every omitted date
    # of the actual portfolio returns. Again get the Monte Carlo permutation
    # shuffles created first so that they will all be the same every time 
    # shuffle() is called.
    ##

    # --- shape: #dates - 1
    jindexrange = np.arange( cashreturns.shape[0] - 1 )

    # --- "shape": #shuffles x (#dates - 1)
    jshuffles = [  np.random.choice( jindexrange, jindexrange.shape[0], replace=False ) for i in range(numshuffles) ]

    # --- "shape": #dates x (#dates - 1)
    jsamples = [ np.delete( indexrange, i, axis=0 ) for i in indexrange ]

    print( 'Computing the corrected confidence intervals...' )

    # --- shape: #dates
    jdist = np.array(
        Parallel(n_jobs=cpu_count())(
            delayed(ia.shuffle)(js, jshuffles, betas, positions, securityreturns, cashreturns, benchmarkreturns, justabnormal=True)
            for js in tqdm(jsamples)
        )
    )

    print('')

    lowerbnd95pct, upperbnd95pct = ia.acceleration(jdist, anndist, lessppf, numsamples, alpha)

    print( 'Return Statistics:' )
    print('')
    print(
        '  Total Annualized Portfolio Return: {:.1f}%'.format(
            100 * ( np.exp( factor * meanactuallogtotreturns ) - 1.0 )
        ) 
    )
    print(
        '  Annualized Cash Return: {:.1f}%'.format(
            100 * ( np.exp( factor * np.mean( np.log( cashreturns ) ) ) - 1.0 )
        ) 
    )
    print( "  Bias-Corrected Annualized Insider's Alpha: {:.1f}%".\
        format( 100.0 * ( np.exp( factor * ( 2.0 * np.mean( meanactuallogtotreturns - meanlogexpectedreturns ) - np.mean(dist) ) ) - 1.0 )  )
    )
 
    print( "  Annualized Insider's Alpha BCa Bootstrap {:.1f}% Confidence Level Lower Bound: {:.1f}%".\
        format( 100 * (1-alpha), lowerbnd95pct )
    )

    print( "  Annualized Insider's Alpha BCa Bootstrap {:.1f}% Confidence Level Upper Bound: {:.1f}%".\
        format(  100 * (1-alpha), upperbnd95pct )
    )

    #-------------------------------------------------------------------------------------------------------------------------

    ##
    # Plot the cumulative portfolio return and the cumulative benchmark and cash returns.
    ##

    cumreturn = np.exp( np.cumsum( actuallogtotreturns ) )
    cumbenchmarkreturn = np.exp( np.cumsum( np.log( benchmarkreturns ) ) )
    cumcashreturn = np.exp( np.cumsum( np.log( cashreturns ) ) )

    ia.chart(
        dates, cumreturn, 'Traded Portfolio', cumbenchmarkreturn, 'Benchmark', series3=cumcashreturn, seriestype3='Cash',
        title='Cumulative Returns', note=None
    )

    #-------------------------------------------------------------------------------------------------------------------------

    ##
    # Plot the portfolio return data as a cdf. Without any resort to cash we would expect the distribution
    # to be rather lognormal--- as is sometimes assumed in scholarly theories of finance. So logs are used here.
    # However even when there is no resort to cash the distribution may have fatter than lognormal tails, and
    # when there is a substantial resort to cash the mixed-distributon aspect is apparent.
    ##

    mu = np.mean(actuallogtotreturns)
    sigma = np.std(actuallogtotreturns)
    note = ' $\mu$: {:.3f}\n $\sigma$: {:.3f}'.format( mu, sigma )
    ia.plot_cdf(actuallogtotreturns, None, 'Distribution of Portfolio Returns', 'Log of {} Total Return'.format(frequency.title()), mu, note)

    #-------------------------------------------------------------------------------------------------------------------------

    ##
    # Plot the Monte Carlo permutation distribution of expected-return-adjusted cumulative returns, the mean of which is the 
    # insider's alpha. It should be pleasingly nearly lognormal because it is a distribution of a mean-like statistic.
    ##

    statistic = 100.0 * ( np.exp( factor * np.mean( meanactuallogtotreturns - meanlogexpectedreturns ) ) - 1.0 )
    sigma = 100.0 * ( np.exp( factor * np.std( meanactuallogtotreturns - meanlogexpectedreturns ) ) - 1.0 )
    note = " Insider's Alpha (uncorrected): {:.1f}%\n $100 (e^{{{}\sigma}}-1)$: {:.1f}%".format( statistic, int(factor), sigma )
    ia.plot_cdf(
        meanactuallogtotreturns - meanlogexpectedreturns, factor, 'Monte Carlo Permutations of Expected-Return-Adjusted Returns',
        'Annualized Adjusted Returns', statistic, note
    )

    #-------------------------------------------------------------------------------------------------------------------------

    ##
    # Plot the Monte Carlo bootstrap sampling distribution of the insider's alphas. It too should be pleasingly nearly lognormal
    # because it is a distribution of mean.
    ##

    statistic = 100.0 * ( np.exp( factor * np.mean( dist ) ) - 1.0 )
    sigma = 100.0 * ( np.exp( factor * np.std( dist ) ) - 1.0 )
    note = " Mean (uncorrected): {:.1f}%\n $100 (e^{{{}\sigma}}-1)$: {:.1f}%".format( statistic, int(factor), sigma )
    ia.plot_cdf(
        meanactuallogtotreturns - meanlogexpectedreturns, factor, "Bootstrap Sampling Distribution of Insider's Alphas",
        'Annualized Abnormal Returns', statistic, note
    )

    #-------------------------------------------------------------------------------------------------------------------------

compute('Audit_mom_french_indu10.csv', numsamples=1000, numshuffles=1000)
